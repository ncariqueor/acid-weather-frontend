import React, {Component} from 'react';
import GoogleMapReact from 'google-map-react';
import Modal from 'react-modal';
import ReactLoading from 'react-loading';
import Skycons from 'react-skycons';
import './Map.css';
import endpoints from '../conf/endpoints';

class MapContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            modalIsOpen: false,
            modalWeatherIsOpen: false,
            temperature: 0,
            summary: '',
            country: '',
            capital: '',
            icon: 'RAIN',
            error: false,
            errorMessage: ''
        };
    }

    static defaultProps = {
        center: {
            lat: -7.53,
            lng: -49.02
        },
        zoom: 0,

    };

    _onClick = async ({x, y, lat, lng, event}) => {
        this.openModal();

        try {
            const response = await fetch(`${endpoints.url}/${lat}/${lng}`, {
                method: 'GET',
                'Content-Type': 'application/json'
            });

            if (response.ok) {
                const weather = await response.json();

                this.setState({
                    temperature: weather.temperature.toFixed(0),
                    country: weather.country,
                    capital: weather.countryCapital,
                    summary: weather.summary,
                    icon: weather.icon,
                    error: false
                });
            } else {
                this.setState({
                    error: true,
                    errorMessage: 'Problemas con el servidor de clima. Por favor, inténtelo más tarde o seleccione otra ubicación. Si el problema persiste, por favor notificar al administrador.'
                });
            }
        } catch (e) {
            alert(`Cannot get response ${e}`);
        }

        this.closeModal();

        this.openWeatherModal();
    };

    closeModal = () => {
        this.setState({
            modalIsOpen: false
        });
    };

    closeWeatherModal = () => {
        this.setState({
            modalWeatherIsOpen: false
        });
    };

    openModal = () => {
        this.setState({
            modalIsOpen: true
        });
    };

    openWeatherModal = () => {
        this.setState({
            modalWeatherIsOpen: true
        });
    };


    render() {
        const {
            error,
            errorMessage,
            country,
            capital,
            summary,
            temperature,
            icon
        } = this.state;

        let div = '';

        if (error) {
            div = (
                <div>
                    <p className={'info'}>Error: {errorMessage}</p>

                </div>
            );
        } else {

            let iconFormat = '';

            if (icon.toString().includes('-')) {

                iconFormat = icon.toString().replace(/-/g, '_');

                iconFormat = iconFormat.toUpperCase();

            } else {
                iconFormat = icon.toString().toUpperCase();
            }

            div = (
                <div>
                    <Skycons
                        color='#61dafb'
                        icon={iconFormat}
                    />
                    <hr></hr>
                    <p className={'info'}>Pais: {country}</p>
                    <p className={'info'}>Capital: {capital}</p>
                    <p className={'info'}>Tiempo: {summary}</p>
                    <p className={'info'}>Temperatura: {temperature} ºC</p>
                </div>
            )
        }

        return (
            <div style={{height: '90vh', width: '100%'}}>

                <GoogleMapReact
                    language={'en'}
                    bootstrapURLKeys={{key: 'AIzaSyBcrsM0cJ33-IqiAy6DGaZqJyO7zbCnS6U'}}
                    defaultCenter={this.props.center}
                    defaultZoom={this.props.zoom}
                    options={
                        {
                            scrollwheel: false,
                            zoomControl: false,
                            gestureHandling: 'greedy',
                            fullscreenControl: false,
                            disableDoubleClickZoom: true
                        }
                    }
                    onClick={this._onClick}
                />

                <Modal
                    isOpen={this.state.modalIsOpen}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    ariaHideApp={false}
                    shouldCloseOnOverlayClick={false}
                >

                    <ReactLoading type={'bubbles'} color={'#61dafb'} height={200} width={100}/>
                </Modal>

                <Modal
                    isOpen={this.state.modalWeatherIsOpen}
                    onRequestClose={this.closeWeatherModal}
                    style={customWeatherStyles}
                    ariaHideApp={false}
                >
                    {div}
                </Modal>
            </div>
        );
    }
}

const customStyles = {
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.6)'
    },
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        backgroundColor: 'transparent',
        border: 'transparent',
        transform: 'translate(-50%, -50%)'
    }
};

const customWeatherStyles = {
    overlay: {
        backgroundColor: 'rgba(0, 0, 0, 0.6)'
    },
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        backgroundColor: '#282c34',
        border: 'transparent',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

export default MapContainer;

