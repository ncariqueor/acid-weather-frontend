import React, {Component} from 'react';
import MapContainer from "./Map/Map";

class App extends Component {
    render() {
        return (
            <div>
                <div>
                    <h1>Acid Weather</h1>
                </div>
                <div style={{width: '100%'}}>
                    <MapContainer/>
                </div>
            </div>
        );
    }
}

export default App;
